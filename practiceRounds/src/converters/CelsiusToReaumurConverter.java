package converters;

import enums.ConverterEnums.conversionType;
import interfaces.IConverter;

public class CelsiusToReaumurConverter implements IConverter {

	@Override
	public double convert(String input) {
		String numString = input.substring(0, input.length()-2);
		double numDouble = Double.parseDouble(numString);
		double reaumur = numDouble * .8;
		return reaumur;
	}

	@Override
	public conversionType getConversionType() {
		// TODO Auto-generated method stub
		return conversionType.celsius_to_reaumur;
	}

}
