package converters;

import enums.ConverterEnums.conversionType;
import interfaces.IConverter;

public class CelsiusToKelvinConverter implements IConverter {

	@Override
	public double convert(String input) {
		String numString = input.substring(0, input.length()-1);
		double numDouble = Double.parseDouble(numString);
		double kelvin = numDouble + 273.15;
		return kelvin;
	}

	@Override
	public conversionType getConversionType() {
		// TODO Auto-generated method stub
		return conversionType.celsius_to_kelvin;
	}

}
