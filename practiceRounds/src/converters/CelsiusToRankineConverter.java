package converters;

import enums.ConverterEnums.conversionType;
import interfaces.IConverter;

public class CelsiusToRankineConverter implements IConverter {

	@Override
	public double convert(String input) {
		String numString = input.substring(0, input.length()-2);
		double numDouble = Double.parseDouble(numString);
		double rankine = ((numDouble * 1.8) + 491.67);
		return rankine;
	}

	@Override
	public conversionType getConversionType() {
		// TODO Auto-generated method stub
		return conversionType.celsius_to_rankine;
	}

}

