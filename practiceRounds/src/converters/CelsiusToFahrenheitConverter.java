package converters;

import enums.ConverterEnums.conversionType;
import interfaces.IConverter;

public class CelsiusToFahrenheitConverter implements IConverter {

	@Override
	public double convert(String input) {
		String numString = input.substring(0, input.length()-1);
		double numDouble = Double.parseDouble(numString);
		double fahrenheit = (numDouble * 1.8) + 32.0;
		return fahrenheit;
	}

	@Override
	public conversionType getConversionType() {
		// TODO Auto-generated method stub
		return conversionType.celsius_to_fahrenheit;
	}

}
