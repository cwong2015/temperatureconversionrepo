package converters;

import enums.ConverterEnums.conversionType;
import interfaces.IConverter;

public class BaseConverter implements IConverter {

	@Override
	public double convert(String input) {
		double celsius = 0;

		if (input.charAt(input.length() - 1) == 'f')
			celsius = fahrenheitToCelsius(input);

		else if (input.charAt(input.length() - 1) == 'k')
			celsius = kelvinToCelsius(input);

		else if (input.charAt(input.length() - 1) == 'c')
			celsius = celsiusToCelsius(input);

		else if (input.substring(input.length() - 2).equals("re"))
			celsius = reaumurToCelsius(input);

		else if (input.substring(input.length() - 2).equals("ra"))
			celsius = rankineToCelsius(input);

		return celsius;

	}

	private double rankineToCelsius(String input) {
		String numString;
		double numDouble;
		double celsius;
		numString = input.substring(0, input.length() - 2);
		numDouble = Double.parseDouble(numString);
		celsius = ((numDouble- 491.67) * .555);
		return celsius;
	}

	private double reaumurToCelsius(String input) {
		String numString;
		double numDouble;
		double celsius;
		numString = input.substring(0, input.length() - 2);
		numDouble = Double.parseDouble(numString);
		celsius = numDouble * 1.25;
		return celsius;
	}

	private double celsiusToCelsius(String input) {
		String numString;
		double numDouble;
		double celsius;
		numString = input.substring(0, input.length() - 1);
		numDouble = Double.parseDouble(numString);
		celsius = (numDouble);
		return celsius;
	}

	private double kelvinToCelsius(String input) {
		String numString;
		double numDouble;
		double celsius;
		numString = input.substring(0, input.length() - 1);
		numDouble = Double.parseDouble(numString);
		celsius = (numDouble - 273.15);
		return celsius;
	}

	private double fahrenheitToCelsius(String input) {
		String numString;
		double numDouble;
		double celsius;
		numString = input.substring(0, input.length() - 1);
		numDouble = Double.parseDouble(numString);
		celsius = (numDouble - 32.0) / 1.8;
		return celsius;
	}

	@Override
	public conversionType getConversionType() {
		return conversionType.anything_to_celsius;
	}

}
