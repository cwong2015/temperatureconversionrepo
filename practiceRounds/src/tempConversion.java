import interfaces.IConverter;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import converters.BaseConverter;
import converters.CelsiusToFahrenheitConverter;
import converters.CelsiusToKelvinConverter;
import converters.CelsiusToRankineConverter;
import converters.CelsiusToReaumurConverter;

import java.text.DecimalFormat;

public class tempConversion implements ActionListener {
	double fahrenheit;
	JButton convertButton;
	JButton resetButton;
	JLabel convertLabel, equalsLabel, toLabel;
	JPanel wholePanel;
	JTextField textField, resultTextField;
	JComboBox<String> comboBox = new JComboBox<>(new String[] { "fahrenheit",
			"celsius", "kelvin", "r�aumur", "rankine" });

	public JPanel createContentPane() {

		wholePanel = new JPanel();
		wholePanel.setLayout(null);

		resultTextField = new JTextField("");
		resultTextField.setSize(150, 25);
		resultTextField.setLocation(285, 22);

		convertLabel = new JLabel("Enter temperature to Convert");
		convertLabel.setSize(500, 50);
		convertLabel.setLocation(145, 135);
		convertLabel.setForeground(Color.blue);

		equalsLabel = new JLabel("=");
		equalsLabel.setSize(400, 40);
		equalsLabel.setLocation(260, 15);
		equalsLabel.setFont(new Font("New Times Roman", Font.BOLD, 15));

		toLabel = new JLabel("to");
		toLabel.setSize(40, 40);
		toLabel.setLocation(120, 15);

		comboBox.setSelectedIndex(-1);
		comboBox.addActionListener(this);
		comboBox.setEditable(true);
		comboBox.setSize(100, 25);
		comboBox.setLocation(150, 22);

		convertButton = new JButton("Convert!");
		convertButton.setSize(100, 33);
		convertButton.setLocation(125, 175);
		convertButton.setActionCommand("ConvertValues");
		convertButton.addActionListener(this);

		resetButton = new JButton("Reset");
		resetButton.setSize(100, 33);
		resetButton.setLocation(240, 175);
		resetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				textField.setText("");
				resultTextField.setText("");
				new tempConversion();
			}

		});

		textField = new JTextField("");
		textField.setSize(100, 25);
		textField.setLocation(10, 20);

		wholePanel.add(convertLabel);
		wholePanel.add(equalsLabel);
		wholePanel.add(convertButton);
		wholePanel.add(resetButton);
		wholePanel.add(comboBox);
		wholePanel.add(textField);
		wholePanel.add(toLabel);
		wholePanel.add(resultTextField);

		return wholePanel;

	}

	public static void main(String[] args) {
		frameMaker();

	}

	private static void frameMaker() {
		JFrame frame = new JFrame("Temperature Conversion");

		tempConversion demo = new tempConversion();
		frame.setContentPane(demo.createContentPane());
		frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 350);
		frame.setVisible(true);
		frame.getContentPane().setBackground(Color.pink);
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getActionCommand().equals("ConvertValues")) {
			try {
				ConvertValues();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		if (e.getSource().equals(convertButton)) {
			System.out.println("Conversion transacted!");
		}
		if (e.getSource().equals(comboBox)) {
			resultTextField.setText("");
		}
		//
		// try {
		// ConvertValues(e);
		// } catch (Exception e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		// // } else if (selectedChoice.equals(comboBox).getItemAt(2))){
		// //
		// // }

	}

	private void ConvertValues() throws Exception {
		// JComboBox<String> combo = (JComboBox<String>) e.getSource();
		JComboBox<String> combo = comboBox;
		String selectedChoice = (String) combo.getSelectedItem();
		String input = textField.getText().toLowerCase();
		IConverter converter[] = { new BaseConverter(),
				new CelsiusToFahrenheitConverter(),
				new CelsiusToKelvinConverter(),
				new CelsiusToReaumurConverter(),
				new CelsiusToRankineConverter()};

		if (selectedChoice.equals((comboBox).getItemAt(0))) {

			try {
				String baseResult = "" + converter[0].convert(input);
				double result = converter[1].convert(baseResult);
				String stringResult = "" + result;
				result = decimalFormat(stringResult);
				resultTextField.setText(result + "� F");

			} catch (Exception ex) {
				throw ex;
			}

		} else if (selectedChoice.equals((comboBox).getItemAt(1))) {
			try {
				double baseResult = converter[0].convert(input);
				String stringResult = "" + baseResult;
				baseResult = decimalFormat(stringResult);
				resultTextField.setText(baseResult + "� C");

			} catch (Exception ex) {
				throw ex;
			}
		} else if (selectedChoice.equals((comboBox).getItemAt(2))) {

			try {
				String baseResult = "" + converter[0].convert(input);
				double result = converter[2].convert(baseResult);
				String stringResult = "" + result;
				result = decimalFormat(stringResult);
				resultTextField.setText(result + " K");

			} catch (Exception ex) {
				throw ex;
			}
		} else if (selectedChoice.equals((comboBox).getItemAt(3))) {
			try {
				String baseResult = "" + converter[0].convert(input);
				double result = converter[3].convert(baseResult);
				String stringResult = "" + result;
				result = decimalFormat(stringResult);
				resultTextField.setText(result + "� R�aumur ");
			} catch (Exception ex) {
				throw ex;
			}
		}
		else if (selectedChoice.equals((comboBox).getItemAt(4))) {
			try {
				String baseResult = "" + converter[0].convert(input);
				double result = converter[4].convert(baseResult);
				String stringResult = "" + result;
				result = decimalFormat(stringResult);
				resultTextField.setText(result + "� R");
			} catch (Exception ex) {
				throw ex;
			}
		}
	}

	private double decimalFormat(String stringResult) {
		DecimalFormat df = new DecimalFormat("0.00");
		df.setMaximumFractionDigits(2);
		double parsedResult = Double.parseDouble(stringResult);
		parsedResult = Double.parseDouble(df.format(parsedResult));
		return parsedResult;
	}
}