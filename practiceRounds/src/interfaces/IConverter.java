package interfaces;

import enums.ConverterEnums.conversionType;

public interface IConverter {
public conversionType getConversionType();
public double convert(String input);
}
